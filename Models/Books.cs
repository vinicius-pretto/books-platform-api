using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace books_platform_api.Models
{
    [Table("books")]
    public class Books
    {
        [Key, Column("id")]
        public int Id { get; set; }

        [Column("name")]
        public string name { get; set; }

        [Column("description")]
        public string description { get; set; }

        [Column("picture_url")]
        public string pictureUrl { get; set; }

        [Column("author")]
        public string author { get; set; }

        [Column("language")]
        public string language { get; set; }

        [Column("publisher")]
        public string publisher { get; set; }

        [Column("year")]
        public int year { get; set; }

        [Column("pages_number")]
        public int pagesNumber { get; set; }
    }
}