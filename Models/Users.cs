using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace books_platform_api.Models
{
    [Table("users")]
    public class Users
    {
        [Key, Column("id")]
        public int Id { get; set; }
        [Column("first_name")]
        public string FirstName { get; set; }
        [Column("last_name")]
        public string LastName { get; set; }
        [Column("email")]
        public string Email { get; set; }
        [Column("birth_date")]
        public string BirthDate { get; set; }
        [Column("telephone")]
        public string Telephone { get; set; }
        [Column("role")]
        public string Role { get; set; }
        [Column("password")]
        public string Password { get; set; }
    }
}