﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace books_platform_api.Models
{
    public partial class BooksPlatformContext : DbContext
    {
        public BooksPlatformContext(DbContextOptions<BooksPlatformContext> options): base(options)
        {}

        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<Books> Books { get; set; }
    }
}
