using Microsoft.AspNetCore.Mvc;
using System.Linq;
using books_platform_api.Models;

namespace books_platform_api.Controllers
{   
    [Route("/v1/auth")]
    public class AuthController : Controller
    {
        private BooksPlatformContext dbContext;
        public AuthController(BooksPlatformContext booksPlatformContext)
        {
            dbContext = booksPlatformContext;
        }

        [HttpPost]
        public IActionResult Login([FromBody] UserCredentials userCredentials)
        {
            Users user = dbContext.Users.SingleOrDefault(usr => 
                (usr.Email == userCredentials.Username) && 
                (usr.Password == userCredentials.Password)
            );
            if (user == null) {
                return Unauthorized();
            }
            return Ok(user);
        }
    }

    public class UserCredentials
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}