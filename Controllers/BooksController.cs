using Microsoft.AspNetCore.Mvc;
using System.Linq;
using books_platform_api.Models;

namespace books_platform_api.Controllers
{   
    [Route("/v1/books")]
    public class BooksController : Controller
    {
        private BooksPlatformContext dbContext;

        public BooksController(BooksPlatformContext booksPlatformContext)
        {
            dbContext = booksPlatformContext;
        }

        
        [HttpPost]
        public void CreateBook([FromBody] Books book)
        {
            dbContext.Books.Add(book);
            dbContext.SaveChanges();
        }

        [HttpGet]
        public Books[] GetAllBooks()
        {
            return dbContext.Books.ToArray();
        }

        [HttpGet("{id}")]
        public Books GetBookById(int id)
        {
            return dbContext.Books.SingleOrDefault(book => book.Id == id);
        }

        
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Books bookToUpdate)
        {
            Books existingBook = dbContext.Books.Find(id);

            if (existingBook != null) {
                bookToUpdate.Id = id;
                dbContext.Books.Update(bookToUpdate);
                dbContext.SaveChanges();
            }
        }

        [HttpDelete("{id}")]
        public void RemoveBook(int id)
        {   
            Books book = dbContext.Books.Find(id);

            if (book != null) {
                dbContext.Books.Remove(book);
                dbContext.SaveChanges();
            }
        }

    }
}