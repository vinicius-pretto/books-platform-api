using Microsoft.AspNetCore.Mvc;
using System.Linq;
using books_platform_api.Models;

namespace books_platform_api.Controllers
{
    [Route("/v1/users")]
    public class UsersController : Controller
    {
        private BooksPlatformContext dbContext;
        public UsersController(BooksPlatformContext booksPlatformContext)
        {
            dbContext = booksPlatformContext;
        }

        [HttpPost]
        public void Post([FromBody] Users user)
        {
            dbContext.Users.Add(user);
            dbContext.SaveChanges();
        }

        [HttpGet]
        public Users[] Get()
        {
            return dbContext.Users.ToArray();
        }

        [HttpGet("{id}")]
        public Users Get(int id)
        {
            return dbContext.Users.SingleOrDefault(user => user.Id == id);
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Users userToUpdate)
        {
            Users existingUser = dbContext.Users.Find(id);

            if (existingUser != null) {
                userToUpdate.Id = id;
                dbContext.Users.Update(userToUpdate);
                dbContext.SaveChanges();
            }
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {   
            Users user = dbContext.Users.Find(id);

            if (user != null) {
                dbContext.Users.Remove(user);
                dbContext.SaveChanges();
            }
        }

    }
}