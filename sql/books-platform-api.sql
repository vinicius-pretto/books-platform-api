CREATE DATABASE books_platform;

USE books_platform;

-- users
CREATE TABLE users (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  first_name VARCHAR(32) NOT NULL,
  last_name VARCHAR(32) NOT NULL,
  email VARCHAR(64) NOT NULL,
  birth_date DATE NOT NULL,
  telephone VARCHAR(16) NOT NULL,
  role VARCHAR(32) NOT NULL,
  password TEXT NOT NULL 
);

INSERT INTO users (
  first_name, 
  last_name, 
  email, 
  birth_date, 
  telephone, 
  role, 
  password
) VALUES (
  'Vinicius',
  'Pretto',
  'vinicius@example.com',
  '1989-05-18',
  '512222-2222',
  'ADMIN',
  'vinicius'
);

-- books
CREATE TABLE books (
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(64) NOT NULL,
  description TEXT NOT NULL,
  picture_url TEXT NOT NULL,
  author VARCHAR(64) NOT NULL,
  language VARCHAR(64) NOT NULL,
  publisher VARCHAR(64) NOT NULL,
  year INTEGER NOT NULL,
  pages_number INTEGER NOT NULL 
);