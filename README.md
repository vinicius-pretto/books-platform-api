# Biblioteca 

## Trabalho de Programação para Internet III

A proposta deste projeto é fazer uma plataforma para reservar livros

## Qual é o problema que o projeto se propõe a resolver?

Trabalho em uma empresa que possui um espaço com livros, os quais podem ser reservados por colaboradores.  
Para organizar e gerenciar o uso desses livros, resolvi propor uma plataforma na qual é possível cadastrar e reservar livros.

## Requisitos funcionais

* Login
* Cadastro de usuários
* Cadastro de livros
* Reserva de livros 
* Devolução de livros
* Visualização das informações dos livros, ex: author, ano de lançamento, idioma, etc
* Histórico de reservas dos livros

## Requisitos não funcionais

* Usuários com perfil (`USER`) podem realizar reservas de livros
* Cada usuário pode reservar até **3 livros**
* O prazo máximo de entrega do livro é de **1 mês**
* Usuários com perfil (`ADMIN`) podem realizar operações em entidades de Usuários e Livros (`CRUD`) 

## [Prototipos](https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Fproto%2FR2ZpMG10Pg5Lo6kgQKmwRA2P%2FEbooks-Library%3Fnode-id%3D0%253A1%26scaling%3Dmin-zoom)

Os prototipos foram feitos com a ferramenta [Figma](https://www.figma.com/)
